<?php
declare(strict_types=1);

namespace Mjay\HyperfHelper\Lib;

use Hyperf\HttpServer\Contract\ResponseInterface as BaseResponseInterface;

interface ResponseInterface extends BaseResponseInterface
{
    public function error($code, ?string $message = "", ?array $data = [], ?bool $realCode = false): array;

    public function success($data = [], ?string $msg = ""): array;
}