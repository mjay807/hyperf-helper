<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/25
 * Time: 22:21
 * Brief:
 * docs:.
 */

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Mjay\HyperfHelper\Lib;

use Mjay\HyperfHelper\Constants\Lib\MigrationConstants;
use Exception;
use Hyperf\Database\Migrations\Migration;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Schema\Schema;

abstract class MigrationAbstract extends Migration
{
    /**
     * @var string 表名
     */
    protected string $tableName;

    /**
     * @var bool 是否新增时间信息
     */
    protected bool $isAddTimestamp = true;

    /**
     * @param Blueprint $table 表类
     */
    abstract public function exec(Blueprint $table): void;

    public function up(): void
    {
        if (! Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                // 创建id
                $table->bigIncrements('id')->comment('ID');
                // 创建时间
                if ($this->isAddTimestamp) {
                    $this->insertTimeList($table);
                }
            });
        }
        Schema::table($this->tableName, function (Blueprint $table) {
            $this->exec($table);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->tableName);
    }

    /**
     * @param Blueprint $table 表
     */
    public function insertTimeList(Blueprint $table): void
    {
        // 创建时间
        $table->integer('created_time', unsigned: true)->default(0)->comment('创建时间');
        // 创建人
        $table->integer('created_user', unsigned: true)->default(0)->comment('创建人');
        // 更新时间
        $table->integer('updated_time', unsigned: true)->default(0)->comment('更新时间');
        // 更新人
        $table->integer('updated_user', unsigned: true)->default(0)->comment('更新人');
        // 删除时间
        $table->integer('deleted_time', unsigned: true)->default(0)->comment('删除时间');
        // 删除人
        $table->integer('deleted_user', unsigned: true)->default(0)->comment('删除人');
    }

    /**
     * @param Blueprint $table 表
     * @param int $indexType 索引类型 index primary unique
     * @param array $indexColumn 索引字段数组
     * @throws Exception
     */
    public function setIndex(Blueprint $table, int $indexType, array $indexColumn): void
    {
        $indexTypeStr = MigrationConstants::INDEX_ARR[$indexType];

        if (empty($indexColumn)) {
            return;
        }
        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        if (! $sm) {
            return;
        }
        $indexesFound = $sm->listTableIndexes($this->tableName); // 查询索引
        $key = strtolower($this->tableName) . '_';
        foreach ($indexColumn as $value) {
            $key .= strtolower($value) . '_';
        }
        $key .= $indexTypeStr; // 索引键名 为 表明_字段名_索引类型
        if (! array_key_exists($key, $indexesFound)) {
            $table->{$indexTypeStr}($indexColumn); // 创建索引
        }
    }


    /**
     * @param string $column
     * @return bool
     * 查询是否含有某个字段
     */
    public function hasColumn(string $column): bool
    {
        return Schema::hasColumn($this->tableName, $column);
    }
    /**
     * @param array $columns
     * @return bool
     * 查询是否含有某些字段
     */
    public function hasColumns(array $columns): bool
    {
        return Schema::hasColumns($this->tableName, $columns);
    }
}
