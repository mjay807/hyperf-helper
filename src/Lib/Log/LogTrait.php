<?php


namespace Mjay\HyperfHelper\Lib\Log;

use Hyperf\Utils\Coroutine;
use Hyperf\Utils\HigherOrderTapProxy;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Trait LogTrait
 * @package App\Lib\Log
 * @property Log $logger
 */
trait LogTrait
{
    private string $logName;

    /**
     * @param $name
     *
     * @return Log|HigherOrderTapProxy|mixed|void|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __get($name)
    {
        if ($name == 'logger') {
            return $this->callLogGet($name);
        }
        $reflectionClass = new \ReflectionClass(self::class);
        if($reflectionClass->getParentClass()){
            return parent::__get($name);
        }
    }

    /**
     * @param $name
     *
     * @return Log|mixed|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function callLogGet($name): mixed
    {
        if ($name == 'logger') {
            $traceInfo = \Swoole\Coroutine::getBackTrace(Coroutine::id(), DEBUG_BACKTRACE_IGNORE_ARGS, 4)[3];
            $className = get_class($this);
            $functionName = $traceInfo['function'];
            unset($this->logger);
            return Log::getInstance($className . ':' . $functionName);
        }
        return null;
    }
}