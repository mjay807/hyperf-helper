<?php


namespace Mjay\HyperfHelper\Lib\Log;

use Hyperf\Logger\LoggerFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Mjay\HyperfHelper\Common\TraceEntity;

/**
 * Class Log
 * @package Scjc\Lib\Log
 * @method static emergency($message, array $context = array());
 * @method static alert($message, array $context = array());
 * @method static critical($message, array $context = array());
 * @method static error($message, array $context = array());
 * @method static warning($message, array $context = array());
 * @method static notice($message, array $context = array());
 * @method static info($message, array $context = array());
 * @method static debug($message, array $context = array());
 * @method static log($level, $message, array $context = array());
 */
class Log
{
    private LoggerInterface $logger;

    public static function get(string $name = 'app')
    {
        return di(LoggerFactory::class)->get($name);
    }

    /**
     * 获取绑定单例日志对象
     *
     * @param string $name
     *
     * @return mixed|Log
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getInstance(string $name = 'app')
    {
        $hasInstance = di()->has(Log::class . ':' . $name);
        if (!$hasInstance) {
            $instance = new self();
            $instance->logger = di(LoggerFactory::class)->get($name);
            di()->set(Log::class . ':' . $name, $instance);
        } else {
            $instance = di(Log::class . ':' . $name);
        }
        return $instance;
    }

    public function __call($name, $arguments)
    {
        //链路追踪写入日志方便排查
        if (TraceEntity::getInstance()->getBeginPath()) {
            $arguments[0] .= ' uri=>' . TraceEntity::getInstance()->getBeginPath();
        }
        if (TraceEntity::getInstance()->getAppIDChain()) {
            $arguments[0] .= ' app_id=>' . TraceEntity::getInstance()->getAppIDChain();
        }
        if (TraceEntity::getInstance()->getTraceId()) {
            $arguments[0] .= ' jc_trace_id=>' . TraceEntity::getInstance()->getTraceId();
        }
        return $this->logger->{$name}(...$arguments);
    }
}