<?php

declare (strict_types=1);

namespace Mjay\HyperfHelper\Lib\Basic;

use Hyperf\Database\Model\Concerns\CamelCase;
use Hyperf\DbConnection\Model\Model;
use Hyperf\ModelCache\Cacheable;
use Hyperf\ModelCache\CacheableInterface;
use Hyperf\Utils\Collection;
use Mjay\HyperfHelper\Lib\Log\LogTrait;

/**
 * @method static Collection indexBy(string|string[]|null $key = 'id', $glue = '') 数组索引
 */
abstract class BaseModel extends Model implements CacheableInterface
{
    use Cacheable,LogTrait,CamelCase;

    /**
     * 获取主键
     * @return string
     */
    final public function getPk(): string
    {
        return $this->primaryKey;
    }
}