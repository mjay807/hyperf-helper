<?php
declare(strict_types=1);

namespace Mjay\HyperfHelper\Lib\Basic;


use Hyperf\Utils\Str;
use Mjay\HyperfHelper\Lib\Log\LogTrait;

/**
 * Class BaseData
 *
 * @package App\Lib\BasicManagerAbstract
 */
abstract class BaseData
{
    use LogTrait;

    /**
     * 是否替换数据下划线
     *
     * @var bool
     */
    protected bool $convertSnake = true;

    /**
     * @var BaseModel
     */
    public BaseModel $model;

    /**
     * 保存数据
     *
     * @param array $data
     * @param int   $id
     * @param null  $pk
     * @param bool  $getLastInsID
     *
     * @return bool|int|mixed
     */
    final public function store(array $data, int $id = 0, $pk = null, bool $getLastInsID = false)
    {
        $model = $this->model;
        $data  = $this->key2Snake($data);
        if ($id) {
            return $model::where([$this->getPk($pk) => $id])
                ->update($data);
        }

        return $getLastInsID ? $model::insertGetId($data) : $model::insert($data);
    }

    /**
     * 保存所有数据
     *
     * @param array $data
     *
     * @return int|null
     */
    public function storeAll(array $data): ?int
    {
        if (count($data) === 0) {
            return null;
        }
        $res = 0;
        foreach ($data as $v) {
            $v   = $this->key2Snake($v);
            $res += (int)$this->model::insert($v);
        }

        return $res;
    }

    /**
     * 删除模型数据
     *
     * @param      $id
     * @param null $pk
     *
     * @return bool
     */
    final public function del($id, $pk = null): bool
    {
        return (bool)$this->model::where([$this->getPk($pk) => $id])->delete();
    }

    /**
     * 获取模型主键
     *
     * @param null $pk
     *
     * @return string|null
     */
    final protected function getPk($pk = null): ?string
    {
        return $pk ? $this->key2Snake($pk) : (new $this->model)->getPk();
    }

    /**
     * 数据键驼峰转下划线
     *
     * @param mixed $data
     *
     * @return string|array
     */
    final protected function key2Snake($data): array|string
    {
        if ($this->convertSnake === false) {
            return $data;
        }
        if (!is_array($data)) {
            return Str::snake($data);
        }
        $res = [];
        foreach ($data as $k => $v) {
            $res[Str::snake($k)] = $v;
        }

        return $res;
    }
}