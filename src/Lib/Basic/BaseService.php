<?php
declare(strict_types=1);

namespace Mjay\HyperfHelper\Lib\Basic;


use Mjay\HyperfHelper\Lib\Log\LogTrait;

/**
 * Class BaseService
 * @package app\lib\BasicManagerAbstract
 */
abstract class BaseService
{
    use LogTrait;

    protected $data;

    public function __call($name, $arguments)
    {
        if(method_exists($this->data, $name)){
            return $this->data->{$name}($arguments);
        }
    }

}