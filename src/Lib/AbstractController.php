<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Mjay\HyperfHelper\Lib;

use Mjay\HyperfHelper\Lib\Log\Log;
use Mjay\HyperfHelper\Lib\Log\LogTrait;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\Container\ContainerInterface;

/**
 * Class AbstractController
 * @package App\Lib
 * @property ContainerInterface container
 * @property RequestInterface   request
 * @property ResponseInterface  response
 * @property Log                $logger
 */
abstract class AbstractController
{
	use LogTrait;

	/**
	 * @var ContainerInterface
	 */
	#[Inject]
	protected ContainerInterface $container;

	/**
	 * @var ResponseInterface
	 */
	#[Inject]
	protected ResponseInterface $response;

	/**
	 * @var RequestInterface
	 */
	#[Inject]
	protected RequestInterface $request;
}
