<?php


namespace Mjay\HyperfHelper\Common;

use Mjay\HyperfHelper\Lib\ResponseInterface;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Response;
use Mjay\HyperfHelper\Constants\BaseCode;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class HttpResponse extends Response implements ResponseInterface
{
    /**
     * 返回成功
     *
     * @param array       $data
     * @param string|null $msg
     *
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function success($data = [], ?string $msg = ''): array
    {
        if (empty($data)) {
            $data = [];
        }
        $code = BaseCode::SUCCESS;
        $msg  = empty($msg) ? BaseCode::getMessage($code) : $msg;

        return $this->setApiData($code, $msg, $data);
    }

    /**
     * 返回失败
     *
     * @param int         $code
     * @param string|null $message
     * @param array|null  $data
     * @param bool|null   $realCode
     *
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function error($code, ?string $message = "", ?array $data = [], ?bool $realCode = false): array
    {
        list($code, $preMsg) = BaseCode::getCodeAndMsg(($code), $data);
        $msg = empty($message) ? $preMsg : $message;

        return $this->setApiData((int)($code), $msg, $data);
    }

    /**
     * 拼接返回数据
     *
     * @param int    $code
     * @param string $msg
     * @param array  $data
     *
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function setApiData(int $code, string $msg = '', $data = []): array
    {
        $data = compact('code', 'msg', 'data');
        //开发模式，返回链路ID方便跟踪日志
        $traceId = TraceEntity::getInstance()->getTraceId();
        if (TraceEntity::isDebug()) {
            $data['debug'] = TraceEntity::getDebuginfo();
            $data['input'] = di(RequestInterface::class)->all();
        }
        $data['appId']      = env('APP_NAME');
        $data['traceId']    = $traceId;

        return $data;
    }
}