<?php

declare(strict_types=1);

use HyperfExtension\Auth\Contracts\AuthManagerInterface;
use Mjay\HyperfHelper\Exception\CodeException;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Redis\Redis;
use Hyperf\Snowflake\IdGeneratorInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Arr;
use HyperfExtension\Auth\Contracts\GuardInterface;
use HyperfExtension\Auth\Contracts\StatefulGuardInterface;
use HyperfExtension\Auth\Contracts\StatelessGuardInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;

if (!function_exists('arr2json')) {
    /**
     * 序列化
     * Created by .
     * Date: 2018/11/21.
     *
     * @param array|object $val
     * @param int          $flag = JSON_UNESCAPED_UNICODE
     *
     * @return null|string
     */
    function arr2json($val, int $flag = 256)
    {
        return is_array($val) || is_object($val) ? json_encode($val, $flag) : $val;
    }
}

if (!function_exists('json2arr')) {
    /**
     * 反序列化
     * Created by .
     * Date: 2018/11/21.
     *
     * @param string|array|object $val
     *
     * @return null|array|object|string
     */
    function json2arr($val)
    {
        if (is_array($val) || $val === null || is_object($val) || is_numeric($val) || is_bool($val)) {
            return $val;
        }
        $tempVal = json_decode($val, true);

        return $tempVal ?? (is_string($val) ? $val : []);
    }
}

if (!function_exists('array_match')) {
    /**
     * 查找数组相同项
     * Created by .
     * Date: 2019/1/11.
     *
     * @param $arr array
     *
     * @return array|false
     */
    function array_match(array $arr)
    {
        $last      = $arr[0];
        $matchData = [];
        $otherData = [];
        array_walk(
            $arr,
            static function ($value) use (&$last) {
                $temp = array_intersect_key($value, $last);
                $last = $temp;
            }
        );
        $last = array_keys($last);
        array_walk(
            $arr,
            static function ($value) use ($last, &$matchData, &$otherData) {
                $temp      = [];
                $otherTemp = [];
                array_walk(
                    $value,
                    static function ($val, $key) use ($last, &$temp, &$otherTemp) {
                        if (in_array($key, $last, true)) {
                            $temp[$key] = $val;
                        } else {
                            $otherTemp[$key] = $val;
                        }
                    }
                );
                if (!empty($temp)) {
                    $matchData[] = $temp;
                }
                if (!empty($otherTemp)) {
                    $otherData[] = $otherTemp;
                }
            }
        );
        if (empty($matchData)) {
            return false;
        }

        return [$matchData, $otherData];
    }
}

if (!function_exists('quick_sort')) {
    /**
     * 快速排序
     * Created by .
     * Date: 2019/2/21.
     *
     * @param bool|int $order
     */
    function quick_sort(array $arr, $order = 0): array
    {
        if (!isset($arr[1])) {
            return $arr;
        }
        $mid        = $arr[0];
        $leftArray  = [];
        $rightArray = [];
        foreach ($arr as $v) {
            if ($v > $mid) {
                $rightArray[] = $v;
            }
            if ($v < $mid) {
                $leftArray[] = $v;
            }
        }
        $leftArray   = quick_sort($leftArray, $order);
        $leftArray[] = $mid;
        $rightArray  = quick_sort($rightArray, $order);
        if ($order) {
            $sortArr = array_reverse([...$leftArray, ...$rightArray]);
        } else {
            $sortArr = [...$leftArray, ...$rightArray];
        }

        return $sortArr;
    }
}

if (!function_exists('multi_quick_sort')) {
    /**
     * 多维数组快速排序
     * Created by .
     * Date: 2019/2/21.
     *
     * @param $arr   array 需要排序的数组
     * @param $key   string 根据那个字段进行排序
     * @param $order mixed 正序还是倒序
     */
    function multi_quick_sort(array $arr, string $key, bool $order = false): array
    {
        if (!isset($arr[1][$key])) {
            return $arr;
        }
        $mid = $arr[0];
        unset($arr[0]);
        $leftArray  = [];
        $rightArray = [];
        foreach ($arr as $v) {
            if ($v[$key] > $mid[$key]) {
                $rightArray[] = $v;
            } else {
                $leftArray[] = $v;
            }
        }
        $leftArray   = multi_quick_sort($leftArray, $key, $order);
        $leftArray[] = $mid;
        unset($mid);
        $rightArray = multi_quick_sort($rightArray, $key, $order);
        if ($order) {
            $sortArr = array_reverse([...$leftArray, ...$rightArray]);
        } else {
            $sortArr = [...$leftArray, ...$rightArray];
        }

        return $sortArr;
    }
}

if (!function_exists('img2base64')) {
    /**
     * 将图片转变为base64
     * Created by .
     * Date: 2019/3/21.
     *
     * @param string $img
     *
     * @return false|string
     */
    function img2base64(string $img = '')
    {
        $file = file_get_contents($img);
        if ($file === false) {
            return false;
        }
        $imageInfo = getimagesize($img);

        return 'data:' . $imageInfo['mime'] . ';base64,' . chunk_split(base64_encode($file));
    }
}

if (!function_exists('fill_zero')) {
    /**
     * 数值填充零
     * Created by .
     * Date: 2019/9/6.
     *
     * @param     $val  string|int|float 数值
     * @param     $bit  int 位数
     * @param int $type 0|1 类型(0: 左填充，1：右填充)
     *
     * @return string
     */
    function fill_zero(&$val, int $bit, int $type = 0): string
    {
        $val = str_pad((string)$val, $bit, '0', $type);

        return $val;
    }
}

if (!function_exists('array_excess')) {
    /**
     * 去除数组多余数据
     * Created by .
     * Date: 2020/11/25.
     */
    function array_excess(array $arr): array
    {
        return array_filter(array_unique($arr));
    }
}

if (!function_exists('to_arr_key_val')) {
    /**
     * 转换数组键对值
     * Created by .
     * Date: 2021-05-24
     *
     * @param array|string      $fromField
     * @param array|string|null $toField
     *
     * @return array
     */
    function to_arr_key_val($fromField, $toField = null): array
    {
        $fieldArr  = [];
        $fromField = is_string($fromField) ? explode(',', $fromField) : $fromField;
        if (is_array($fromField) && $toField === null) {
            foreach ($fromField as $k => $v) {
                if (is_numeric($k)) {
                    $fieldArr[$v] = $v;
                } else {
                    $fieldArr[$k] = $v;
                }
            }
        }
        if (is_array($fromField) && is_array($toField)) {
            $fieldArr = array_combine($fromField, $toField);
        }
        if (is_array($fromField) && is_string($toField)) {
            $fieldArr = array_combine($fromField, array_pad([], count($fromField), $toField));
        }

        return $fieldArr;
    }
}

if (!function_exists('arr2tree')) {
    /**
     * 数组转树形结构
     * Created by .
     * Date: 2021-05-31
     *
     * @param array      $list
     * @param int|string $pid
     * @param string     $pidField
     * @param string     $pkField
     * @param int|null   $maxLevel
     * @param string     $childrenName
     * @param int        $level
     *
     * @return array|null
     */
    function arr2tree(
        array  $list,
               $pid = 0,
        string $pidField = 'pid',
        string $pkField = 'id',
        ?int   $maxLevel = null,
        string $childrenName = 'children',
        int    $level = 1
    ): ?array
    {
        return __arr2tree($list, $pid, $pidField, $pkField, $maxLevel, $childrenName, $level);
    }

    /**
     * 遍历优化(优化后是之前得一倍)
     * Created by .
     * Date: 2021-05-31
     *
     * @param array      $list
     * @param int|string $pid
     * @param string     $pidField
     * @param string     $pkField
     * @param int|null   $maxLevel
     * @param string     $childrenName
     * @param int        $level
     *
     * @return array|null
     */
    function __arr2tree(
        array  &$list,
               $pid = 0,
        string $pidField = 'pid',
        string $pkField = 'id',
        ?int   $maxLevel = null,
        string $childrenName = 'children',
        int    $level = 1
    ): ?array
    {
        if ($maxLevel !== null && $level > $maxLevel) {
            return [];
        }
        $data = [];
        foreach ($list as $k => $val) {
            if (!isset($val[$pidField], $val[$pkField])) {
                continue;
            }
            if ($val[$pidField] === $pid) {
                $temp   = $val + [
                        $childrenName . 'Level' => $level,
                        $childrenName           => __arr2tree(
                            $list,
                            $val[$pkField],
                            $pidField,
                            $pkField,
                            $maxLevel,
                            $childrenName,
                            $level + 1
                        ),
                    ];
                $data[] = $temp;
                unset($list[$k]);
            }
        }

        return $data;
    }
}

if (!function_exists('tree2arr')) {
    /**
     * 树形结构转数组结构
     * Created by .
     * Date: 2021-06-17
     *
     * @param array  $tree
     * @param string $childrenName
     * @param int    $level
     * @param array  $arr
     *
     * @return array|null
     */
    function tree2arr(array $tree, string $childrenName = 'children', int $level = 1, array &$arr = []): ?array
    {
        foreach ($tree as $val) {
            $children = $val[$childrenName] ?? [];
            unset($val[$childrenName]);
            $arr[] = array_merge($val, [$childrenName . 'Level' => $level]);
            if ($children) {
                tree2arr($children, $childrenName, $level + 1, $arr);
            }
        }

        return $arr;
    }
}

if (!function_exists('di')) {
    /**
     * Created by .
     * Date: 2021-07-14
     *
     * @param string|null $id
     *
     * @return mixed|ContainerInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    function di(?string $id = null): mixed
    {
        $container = ApplicationContext::getContainer();
        if ($id) {
            return $container->get($id);
        }

        return $container;
    }
}


if (!function_exists('getIp')) {
    /**
     * 获取IP地址
     *
     * @param $server
     *
     * @return mixed
     */
    function getIp($server)
    {
        if (isset($server['http_client_ip'])) {
            return $server['http_client_ip'];
        } elseif (isset($server['http_x_real_ip'])) {
            return $server['http_x_real_ip'];
        } elseif (isset($server['http_x_forwarded_for'])) {
            //部分CDN会获取多层代理IP，所以转成数组取第一个值
            $arr = explode(',', $server['http_x_forwarded_for']);

            return $arr[0];
        } else {
            return $server['remote_addr'];
        }
    }
}

if (!function_exists('to_array')) {
    /**
     * 任何类型转数组(去重)
     * Created by .
     * Date: 2021-07-20
     *
     * @param mixed|callable $var
     *
     * @return array
     */
    function to_array($var): array
    {
        $var = value($var);

        return array_excess(is_array($var) ? $var : explode(',', $var));
    }
}

if (!function_exists('arrIndexBy')) {
    /**
     * 数组索引
     * Created by .
     * Date: 2021/8/17
     *
     * @param array                 $arr                数组
     * @param string|array|string[] $keys               索引值
     * @param bool                  $isAppend           是否追加数组
     * @param bool                  $columnKeys         数据集键
     * @param string                $keyGlue            联合索引拼接字符串
     * @param int|string|null       $fillNotExistKeyVal 填充不存在的键值(null:不填充)
     *
     * @return array|null
     */
    function arrIndexBy($arr, $keys, bool $isAppend = false, $columnKeys = true, string $keyGlue = '-', $fillNotExistKeyVal = 0): ?array
    {
        $keys = to_array($keys);
        if (empty($keys)) {
            return null;
        }
        /**
         * @var array $columnKeys
         */
        if ($columnKeys !== true) {
            $columnKeys = to_array($columnKeys);
            if (empty($columnKeys)) {
                $columnKeys = true;
            }
        }
        $result = [];
        foreach ($arr as $value) {
            if (!is_array($value)) {
                $value = json_decode(json_encode($value), true);
            }
            $resultKey = implode($keyGlue, arrOnlyKSort($value, $keys, $fillNotExistKeyVal));
            if (is_array($columnKeys)) {
                $value = arrOnlyKSort($value, $columnKeys);
            }
            if ($isAppend) {
                $result[$resultKey] = [...($result[$resultKey] ?? []), ...[$value]];
            } else {
                $result[$resultKey] = $value;
            }
        }

        return $result;
    }
}


if (!function_exists('redis')) {
    /**
     * 获取redis 实例
     *
     * @author .
     * Date: 2023/2/2 0002
     * @return Redis
     */
    function redis(): Redis
    {
        return di(Redis::class);
    }
}

if (!function_exists('logger')) {

    /**
     * 获取logger 实例
     *
     * @author .
     * Date: 2023/2/2 0002
     *
     * @param string $id
     *
     * @return mixed|\Psr\Log\LoggerInterface
     */
    function logger(string $id = 'log'): LoggerInterface
    {
        return di(LoggerFactory::class)->get($id);
    }
}


if (!function_exists('recordError')) {
    /**
     * 记录错误信息
     *
     * @author .
     *
     * @param string|null    $msg
     * @param Throwable|null $throwable
     */
    function recordError(?string $msg = null, ?Throwable $throwable = null, ?string $msgPrefix = null)
    {
        $debugInfo = debug_backtrace();
        $traceInfo = [];
        foreach ($debugInfo as $debug) {
            if (stripos($debug['class'] ?? '', 'App\\') !== false) {
                $traceInfo = $debug;
                break;
            }
        }
        if ($throwable instanceof CodeException && !is_null($throwable->getPrevious())) {
            // 拿到真正错误信息
            $throwable = $throwable->getPrevious();
        }
        $maybeError = [];
        $traces     = $throwable?->getTrace() ?? [];
        foreach ($traces as $i => $trace) {
            if (stripos($trace['class'] ?? '', 'App\\') !== false) {
                $maybeError = $trace;
                break;
            }
        }
        $recordTraceKeys = ['line', 'function', 'class', 'args'];
        $msg             = $msgPrefix ? $msgPrefix . ':' . $msg : $msg;
        logger()->error(
            $msg . ':' . arr2json(
                [
                    'msg'             => $msg,
                    'trace'           => Arr::only($traceInfo, $recordTraceKeys),
                    'file'            => $throwable ? $throwable->getFile() . ':' . $throwable->getLine() : '',
                    'throwMsg'        => $throwable?->getMessage() ?? '',
                    'maybeErrorIndex' => Arr::only($maybeError, $recordTraceKeys),
                ]
            )
        );

    }
}

if (!function_exists('auth')) {
    /**
     * Auth认证辅助方法
     *
     * @param string|null $guard
     *
     * @return GuardInterface|StatefulGuardInterface|StatelessGuardInterface
     */
    function auth(string $guard = null): StatefulGuardInterface|GuardInterface|StatelessGuardInterface
    {
        if (is_null($guard)) $guard = config('auth.default.guard');
        return make(AuthManagerInterface::class)->guard($guard);
    }
}
