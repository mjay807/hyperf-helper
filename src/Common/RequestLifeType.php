<?php


namespace Mjay\HyperfHelper\Common;


use Hyperf\Context\Context;

/**
 * 当前运行环境
 * Class RequestLifeType
 * @package App\Common
 */
final class RequestLifeType
{
    static int $isRest = 1;

    // 暂未接
    static int $isProcess = 2;

    static int $isRpc = 3;

    // 暂未接
    static int $isCron = 4;

    static int $isMQ = 5;


    public static function setLifeType(int $liveType)
    {
        Context::set(RequestLifeType::class, $liveType);
        Context::set(RequestLifeType::class.'_startTime', microtime(true));
    }

    /**
     * 获取生命周期类型
     * @return false|mixed|null
     */
    public static function getLiftType()
    {
        return Context::get(RequestLifeType::class);
    }

    /**
     * 获取开始时间
     * @return false|mixed|null
     */
    public static function getLiftStartTime()
    {
        return Context::get(RequestLifeType::class.'_startTime');
    }

    /**
     * 是否mq
     * @return bool
     */
    public static function isMQ()
    {
        return Context::get(RequestLifeType::class) == self::$isMQ;
    }

    public static function isRest()
    {
        return Context::get(RequestLifeType::class) == 1;
    }

    public static function isRpc()
    {
        return Context::get(RequestLifeType::class) == 3;
    }

    public static function isCron()
    {
        return Context::get(RequestLifeType::class) == 4;
    }
}