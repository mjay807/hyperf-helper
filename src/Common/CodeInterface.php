<?php


namespace Mjay\HyperfHelper\Common;

/**
 * Interface CodeInterface
 * @package Scjc\Lib\Common
 * @method static getMessage(int $code, array $data = []): string
 */
interface CodeInterface
{
}