<?php


namespace Mjay\HyperfHelper\Common;


use Mjay\HyperfHelper\Utils\Context;

class TraceEntity extends ObjectJsonSerialisze
{
    private string $beginPath;

    private string $traceId;

    //链路经过的AppId
    private string $appIDChain;

    private float  $requestStartTime = 0;


    /**
     * 记录外部连接，如sql、rpc等耗时较多的信息 方便排查
     *
     * @var array
     */
    protected array $slowLogConn = [];

    /**
     * @return string
     */
    public function getBeginPath(): string
    {
        return $this->beginPath ?? "cli";
    }

    /**
     * @param string $beginPath
     */
    public function setBeginPath(string $beginPath): void
    {
        $this->beginPath = $beginPath;
    }

    /**
     * @return string
     */
    public function getTraceId(): string
    {
        return $this->traceId ?? "";
    }

    /**
     * @param string $traceId
     */
    public function setTraceId(string $traceId): void
    {
        $this->traceId = $traceId;
    }

    /**
     * @return string
     */
    public function getAppIDChain(): string
    {
        return $this->appIDChain ?? "";
    }

    /**
     * @param string $appIDChain
     */
    public function setAppIDChain(string $appIDChain): void
    {
        $this->appIDChain = $appIDChain;
    }

    /**
     * @return float
     */
    public function getRequestStartTime(): float
    {
        return $this->requestStartTime;
    }

    /**
     * @param float $requestStartTime
     */
    public function setRequestStartTime(float $requestStartTime): void
    {
        $this->requestStartTime = $requestStartTime;
    }


    /**
     * 获取链路实例
     *
     * @return TraceEntity
     */
    public static function getInstance(): TraceEntity
    {
//        if (Context::has(self::class)) {
//            return Context::get(self::class);
//        } else {
//            //没有注入自己的业务Code枚举 默认使用底层
//            $codeObj = new self();
//            Context::set(self::class, $codeObj);
//            return $codeObj;
//        }
        if (Context::has(self::class)) {
            return Context::get(self::class);
        }
        if (Context::has(self::class)) {
            return Context::get(self::class);
        }

        //没有注入自己的业务Code枚举 默认使用底层
        $codeObj = new self();
        Context::set(self::class, $codeObj);

        return $codeObj;
    }

    /**
     * @author XJ.
     * Date: 2022/8/29 0029
     * @return bool
     */
    public static function isDebug(): bool
    {
        return env('APP_ENV', '') !== 'prod'
               || config('open_debug', 0);
    }

    public function slowLogConn(string $type, string $string, $excuteTime = "", ?array $debugInfo = [], false|array $otherInfo = false, bool $isRecord = true)
    {
        $execTime = strpos($excuteTime, 'ms') !== false ? ((float)$excuteTime) : (((float)$excuteTime) * 1000);
        if ($excuteTime) {
            $excuteTime = "excute " . $excuteTime . " ";
        }
        if ($type == "rpc") {
            $debug = [
                'info'  => $excuteTime . $string,
                'debug' => $debugInfo
            ];
        } else if ($otherInfo !== false) {
            $debug = [
                'info'  => $excuteTime . $string,
                'debug' => $otherInfo,
            ];
        } else {
            $debug = $excuteTime . $string;
        }
        if (self::isDebug()) {
            $this->slowLogConn[$type][] = $debug;
        }
    }

    public function getSlowLogConn()
    {
        return $this->slowLogConn;
    }

    /**
     * 获取链路实例
     *
     * @return TraceEntity
     */
    public static function setInstance(?string $tradeId = "", ?string $beginPath = "", ?string $appIdChain = "")
    {
        $codeObj = new self();
        $codeObj->setAppIDChain($appIdChain);
        $codeObj->setTraceId($tradeId);
        $codeObj->setBeginPath($beginPath);
        //设置开始时间
        $codeObj->setRequestStartTime(microtime(true));
        /** @var self $container */
        $container = Context::set(self::class, $codeObj);
        Context::set(self::class, $codeObj);

        return $container;
    }

    public static function getDebuginfo(): array
    {
        $instance        = self::getInstance();
        $data['traceId'] = $instance->getTraceId();
        $data['runtime'] = microtime(true) - $instance->getRequestStartTime();
        $data['memory']  = sprintf("%3.2f", memory_get_usage() / 1024 / 1024) . "M";
        $data['slowLog'] = $instance->getSlowLogConn();

        return $data;
    }

    public static function clearData()
    {
        $instance  = self::getInstance();
        $traceId   = $instance->getTraceId();
        
        $instance->clear();
    }


    public function clear()
    {
        if(Context::has(self::class)){
            Context::destroy(self::class);
        }
        $this->slowLogConn = [];
    }
}