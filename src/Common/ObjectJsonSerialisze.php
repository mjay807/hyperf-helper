<?php


namespace Mjay\HyperfHelper\Common;


use Hyperf\Utils\Contracts\Jsonable;
use JsonSerializable;
use Mjay\HyperfHelper\Constants\BaseCode;
use Mjay\HyperfHelper\Exception\CodeException;

abstract class ObjectJsonSerialisze implements Jsonable, JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toJson(int $options = 0)
    {
        $json = json_encode($this->toArray(), $options);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new CodeException(BaseCode::SYSTEM_ERROR, json_last_error_msg());
        }
        return $json;
    }

    public function toArray()
    {
        $reflect = new \ReflectionObject($this);
        $props = $reflect->getProperties();
        $data = [];
        foreach ($props as $value_p) {
            try {
                $value_p->setAccessible(true);
                $value = $value_p->getValue($this);
                if (is_object($value) && is_subclass_of($value, ObjectJsonSerialisze::class)) {
                    $value->toArray();
                } else {
                    $data[$value_p->getName()] = $value;
                }
            } catch (\Throwable $e) {
                continue;
            }
        }
        return $data;
    }

    public function __toString(): string
    {
        return $this->toJson();
    }
}