<?php

declare (strict_types=1);
namespace Mjay\HyperfHelper\Constants;

use Mjay\HyperfHelper\Common\CodeInterface;
use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
/**
 * 基础code码
 * @version     1.0
 * @package Scjc\Lib\Common
 * @method static getMessage(int $code, array $data = []): string
 */
#[Constants]
class BaseCode extends AbstractConstants implements CodeInterface
{
    /**
     * @Message("操作成功")
     */
    public const SUCCESS = 0;
    /**
     * @Message("系统错误")
     */
    public const SYSTEM_ERROR = 500;
    /**
     * @Message("系统调用错误")
     */
    public const SYSTEM_RPC_ERROR = 501;
    /**
     * 分布式ID解析出错
     * @Message("ID格式错误")
     */
    public const SYSTEM_ID_ERROR = 510;
    /**
     * sql执行错误
     * @Message("sql执行错误")
     */
    public const SQL_QUERY_ERR = 511;
    /**
     * @Message("请求参数错误")
     */
    public const BAD_REQUEST = 400;
    /**
     * @Message("数据验证失败")
     */
    public const VERIFY_FAILED = 422;
    /**
     * @Message("请求方式错误")
     */
    public const METHOD_NOT_ALLOW = 405;
    /**
     * @Message("请求不允许")
     */
    public const FORBIDDEN = 403;
    /**
     * @Message("未授权")
     */
    public const UNAUTHORIZED = 401;
    /**
     * @Message("未找到")
     */
    public const NOT_FOUND = 404;
    /**
     * @Message("配置不存在")
     */
    public const CONFIG_NOT_FOUND = 4004;
    /**
     * @Message("数据库异常")
     */
    public const DB_ERROR = 4005;
    /**
     * @Message("请求授权中心错误")
     */
    public const AUTH_MANAGE_ERROR = 4006;
    /**
     * @Message("获取授权中心url失败")
     */
    public const AUTH_MANAGE_GET_URL_FAIL = 4007;
    /**
     * @Message("获取管理员基础信息失败")
     */
    public const AUTH_GET_ADMIN_BASE_INFO_FAIL = 4008;
    /**
     * @Message("无操作权限")
     */
    public const PERMISSION_FAILED = 4401;
    /**
     * @Message("身份失效，请重新登入")
     */
    public const AUTH_TOKEN_FAILED = 4100;
    /**
     * @Message("请先登录")
     */
    public const API_NOT_LOGIN = 4101;
    /**
     * @Message("暂时无法登录,请联系客服")
     */
    public const USER_LOGIN_FAILED = 4405;
    /**
     * @Message("账户涉嫌违规操作，如有疑问请联系客服人员")
     */
    public const USER_BLACK_LIST = 4406;
    /**
     * @Message("账户注册试用期限已过，请尽快联系客服人员建档")
     */
    public const ACTIVITY_STATUS = 4407;
    /**
     * @Message("账户已停用，如有疑问请联系客服人员")
     */
    public const ACCOUNT_STATUS = 4408;
    /**
     * @Message("账户已停用，如有疑问请联系客服人员")
     */
    public const IS_ACTIVITY = 4409;
    /**
     * @Message("账户已停用，如有疑问请联系客服人员")
     */
    public const IS_LOCK = 4410;
    /**
     * @param int        $code
     * @param array|null $data
     *
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getCodeAndMsg(int $code, ?array $data = []) : array
    {
        $msg = BaseCode::getMessage($code, ...$data);
        if (!empty($msg)) {
            //在基类中定义的，不加APP_NAME
            return [$code, $msg];
        }
        //优先取CodeInterface
        $msg = Di(CodeInterface::class)::getMessage($code, ...$data);
        if (!empty($msg)) {
            $code = env('APP_NAME') . $code;
            return [$code, $msg];
        }
        //优先取CodeInterface没有定义，则考虑合并项目中
        $newCode = env("APP_NAME") . $code;
        if (ApplicationContext::getContainer()->has('ErrorCodeProxy' . $newCode)) {
            $msg = ApplicationContext::getContainer()->get('ErrorCodeProxy' . $newCode)::getMessage($code, ...$data);
            $code = env('APP_NAME') . $code;
        }
        return [$code, $msg];
    }
}