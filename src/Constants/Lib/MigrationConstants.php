<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 00:16
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace Mjay\HyperfHelper\Constants\Lib;

/**
 * 创建表需要用的参数
 */
class MigrationConstants
{

    const UNIQUE = 1;

    const PRIMARY = 2;

    const INDEX = 3;

    const INDEX_ARR = [
        self::UNIQUE => 'unique',
        self::PRIMARY => 'primary',
        self::INDEX => 'index'
    ];

}