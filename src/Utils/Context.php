<?php
namespace Mjay\HyperfHelper\Utils;

use Hyperf\Engine\Coroutine as Co;
use Swoole\Coroutine as SwooleCo;

class Context
{
    /**
     * 是否在子协程中
     * Date: 2021/12/16
     * @return bool
     */
    public static function inSubCoroutine()
    {
        return SwooleCo::getPcid() > 0;
    }

    /**
     * 设置
     * Created by XJ.
     * Date: 2021/12/16
     *
     * @param   string  $id
     * @param           $value
     *
     * @return mixed
     */
    public static function set(string $id, $value)
    {
        if (self::inSubCoroutine()) {
            // 实现子协程嵌套
            Co::getContextFor(SwooleCo::getCid())[$id] = Co::getContextFor(SwooleCo::getPcid())[$id] = $value;
        } else {
            Co::getContextFor(SwooleCo::getCid())[$id] = $value;
        }
        return $value;
    }

    /**
     * 获取
     * Created by XJ.
     * Date: 2021/12/16
     *
     * @param   string  $id
     * @param           $default
     *
     * @return false|mixed|null
     */
    public static function get(string $id, $default = null)
    {
        if (self::inSubCoroutine()) {
            // 子协程中获取父级协程上下文
            return Co::getContextFor(SwooleCo::getPcid())[$id] ?? $default;
        }

        return Co::getContextFor(SwooleCo::getCid())[$id] ?? $default;
    }

    /**
     * 是否存在
     * Created by XJ.
     * Date: 2021/12/16
     *
     * @param   string  $id
     *
     * @return bool
     */
    public static function has(string $id)
    {
        if (self::inSubCoroutine()) {
            // 子协程中获取父级协程上下文
            return isset(Co::getContextFor(SwooleCo::getPcid())[$id]);
        }

        return isset(Co::getContextFor(SwooleCo::getCid())[$id]);
    }

    /**
     * Release the context when you are not in coroutine environment.
     */
    public static function destroy(string $id)
    {
        if (self::inSubCoroutine()) {
            // 子协程中获取父级协程上下文
            unset(Co::getContextFor(SwooleCo::getPcid())[$id]);
        }
        unset(Co::getContextFor(SwooleCo::getCid())[$id]);
    }

    /**
     * Retrieve the value and override it by closure.
     */
    public static function override(string $id, \Closure $closure)
    {
        $value = null;
        if (self::has($id)) {
            $value = self::get($id);
        }
        $value = $closure($value);
        self::set($id, $value);
        return $value;
    }

    /**
     * Retrieve the value and store it if not exists.
     * @param mixed $value
     */
    public static function getOrSet(string $id, $value)
    {
        if (! self::has($id)) {
            return self::set($id, value($value));
        }
        return self::get($id);
    }

    public static function getContainer()
    {
        if (self::inSubCoroutine()) {
            // 子协程中获取父级协程上下文
            return Co::getContextFor(SwooleCo::getPcid());
        }

        return Co::getContextFor(SwooleCo::getCid());
    }
}