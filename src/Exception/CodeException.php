<?php

namespace Mjay\HyperfHelper\Exception;


use Hyperf\Server\Exception\ServerException;
use Mjay\HyperfHelper\Constants\BaseCode;

class CodeException extends ServerException
{
    protected int   $appId;

    protected array $traces;

    /**
     * CodeException constructor.
     *
     * @param int             $code     错误代码
     * @param string|null     $message
     * @param \Throwable|null $previous
     * @param bool            $isRecord 是否记录错误日志
     */
    public function __construct(int $code = BaseCode::SYSTEM_ERROR, string $message = null, \Throwable $previous = null, bool $isRecord = false, ?string $recordErrorPrefix = null)
    {
        list($code, $msg) = BaseCode::getCodeAndMsg($code);
        if (is_null($message)) {
            $message = $msg;
        }
        $this->appId = env("APP_NAME");
        parent::__construct($message, $code, $previous);
        if ($isRecord) {
            // 记录错误信息
            recordError($msg, $this, $recordErrorPrefix);
        }
    }

    /**
     * @return array
     */
    public function getTraces(): ?array
    {
        if (empty($this->traces)) {
            return $this->getTrace();
        }

        return $this->traces;
    }

    /**
     * @param array $traces
     */
    public function setTraces($traces): void
    {
        $this->traces = $traces;
    }


}