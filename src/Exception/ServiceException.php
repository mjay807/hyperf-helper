<?php

namespace Mjay\HyperfHelper\Exception;


use Mjay\HyperfHelper\Constants\BaseCode;
use Throwable;

class ServiceException extends CodeException
{
    /**
     * CodeException constructor.
     *
     * @param int            $code     错误代码
     * @param string|null    $message
     * @param Throwable|null $previous
     * @param bool           $isRecord 是否记录错误日志
     *
     */
    public function __construct(int $code = BaseCode::SYSTEM_ERROR, string $message = null, Throwable $previous = null, bool $isRecord = false, ?string $recordErrorPrefix = null)
    {
        parent::__construct($code, $message, $previous, $isRecord, $recordErrorPrefix);
    }
}