<?php

declare(strict_types=1);

namespace Mjay\HyperfHelper\Exception\Handler;

use Mjay\HyperfHelper\Common\TraceEntity;
use Mjay\HyperfHelper\Constants\BaseCode;
use Mjay\HyperfHelper\Common\CodeInterface;
use Mjay\HyperfHelper\Exception\CodeException;
use Mjay\HyperfHelper\Lib\Log\Log;
use Mjay\HyperfHelper\Lib\ResponseInterface as Response;
use Mjay\HyperfHelper\Utils\Context;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Validation\ValidationException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 处理所有Http服务异常.
 *
 * Class HttpExceptionHandler
 * @property CodeInterface $codeEnum
 */
class HttpExceptionHandler extends ExceptionHandler
{


    /**
     * @var Response
     */
    protected Response $response;

    /**
     * @var CodeInterface
     */
    protected CodeInterface $baseCode;

    public function __construct(Response $response, CodeInterface $baseCode)
    {
        $this->response = $response;
        $this->baseCode = $baseCode;
    }

    /**
     * @param Throwable         $throwable
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function handle(Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        $code = $throwable->getCode();
        if (!$code || !($throwable instanceof CodeException)) {
            $code = BaseCode::SYSTEM_ERROR;
        }
        $msg = $throwable->getMessage();

        if ($throwable instanceof ValidationException) {
            $code = $throwable->validator->errors()->first();
            if (strval(intval($code)) == $code) {
                //如果是错误码
                [$code, $msg] = BaseCode::getCodeAndMsg(intval($code));
            } else {
                if (Context::has('FormRequestErrorCode')) {
                    //如果FormRequest类定义了
                    $code = Context::get('FormRequestErrorCode');
                    [$code, $msg] = BaseCode::getCodeAndMsg($code);
                } else {
                    $code = BaseCode::VERIFY_FAILED;
                }
                $msg = $throwable->validator->errors()->first();
            }
        }else if(!TraceEntity::isDebug() && strlen((string)$code) <= 3 && !preg_match("/[\x7f-\xff]/", $msg)){
            //记录异常日志
            Log::getInstance()->error('HttpExceptionHandler->' . get_class($throwable) . $msg, $throwable->getTrace());
        }

        $data = [];
        $responseData = $this->response->error($code, $msg, $data);
        // 过滤验证提示提示
        if (!($throwable instanceof ValidationException)) {
            if (1) {
            //if (env('APP_ENV') != 'prod') {
                $responseData['exception'] = [[
                    'file' => $throwable->getFile(),
                    'line' => $throwable->getLine(),
                    'message' => $throwable->getMessage(),
                ]];
                $responseData['exception'] = [...$responseData['exception'], ...$throwable->getTrace()];
                $responseData['exceptionAppId'] = env("APP_NAME");
            }
        }
        $result = json_encode($responseData, JSON_UNESCAPED_UNICODE);
        $this->stopPropagation();
        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->withBody(new SwooleStream($result));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
